//
//  BonusAPIApp.swift
//  BonusAPI
//
//  Created by Александра on 14.05.2021.
//

import SwiftUI

@main
struct BonusAPIApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
